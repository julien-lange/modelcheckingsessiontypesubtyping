# Characteristic Formulae for Session Types #

### Organisation ###
* Root folder contains the tool
* ./data contains the data used for the plots
* ./benchmarks contains the session types files used for benchmarks

### Requirements ###

* Haskell (GHC or haskell-platform on Ubuntu & co)
* cabal dependencies: bound, MissingH, cmdargs, prelude-extras, text, parsec, QuickCheck
* mCRL2 (see http://www.mcrl2.org/release/user_manual/download.html)

### Compile ###

* *ghc Checher*
* *ghc RandomTypes*


### Run ###
* See *./Checker --help* for info
* *./Checker -B <path-to-candidate-subtype> <path-to-candidate-supertype>* (replace 'B', by 'P', 'K', or 'G)
* It can also be used "interactively", for example: 
* * ./Checker -iB '?request; !ok; end' 'rec X. ?request ; +{!ok; end , !ko;X}'
* * ./Checker -iB 'rec X . !request; &[?ok;end, ?ko;X, ?error; end]' 'rec X. !request; &[?ok;end, ?ko;X]'
* *./RandomTypes* -- to generate random types (no (command-line) user interface, yet)